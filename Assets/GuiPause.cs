﻿using UnityEngine;
using System.Collections;

public class GuiPause : MonoBehaviour {

	private bool painettu;
	public Texture2D nappi;
	public GUIStyle tyyli;

	// Use this for initialization
	void Start () {
		if(Time.timeScale == 0f) Time.timeScale = 1.0f;
		painettu = false;
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetKeyDown (KeyCode.Escape))
			painettu = true;
	}

	void OnGUI(){

		if (painettu) {
			Screen.showCursor = true;
			Time.timeScale = 0f;
			GameObject.FindWithTag("Player").SendMessage("Disable", SendMessageOptions.DontRequireReceiver);
			if (GUI.Button (new Rect ((Screen.width/2)-50, (Screen.height/2)-50, nappi.width / 3, nappi.height / 3), nappi, tyyli)) {
				Resume();
			}
			GUI.Label(new Rect ((Screen.width/2)-50, (Screen.height/2)-50, nappi.width / 3, nappi.height / 3), "Jatka peliä", tyyli);
			if (GUI.Button (new Rect ( (Screen.width/2)-50, (Screen.height/2)+ 10, nappi.width / 3, nappi.height / 3), nappi, tyyli)) {
				Application.LoadLevel("mainMenu");
			}
			GUI.Label(new Rect ( (Screen.width/2)-50, (Screen.height/2)+ 10, nappi.width / 3, nappi.height / 3), "Lopeta peli", tyyli);
		}
	}
	
	void Resume() {
		painettu = false;
		Time.timeScale = 1.0f;
		Screen.showCursor = false;
		GameObject.FindWithTag("Player").SendMessage("Enable", SendMessageOptions.DontRequireReceiver);
	}
}