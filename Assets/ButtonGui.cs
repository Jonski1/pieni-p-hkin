﻿using UnityEngine;
using System.Collections;

public class ButtonGui : MonoBehaviour {

	public Texture2D nappi;
	public GUIStyle tyyli;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI(){

		if (GUI.Button (new Rect (10, (Screen.height - 100), nappi.width/2, nappi.height / 2), nappi, tyyli)) {
			Application.LoadLevel("mainMenu");
		}
		GUI.Label(new Rect (10, (Screen.height - 100), nappi.width/2, nappi.height / 2), "Takaisin", tyyli);
	}
}
