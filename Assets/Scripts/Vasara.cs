﻿using UnityEngine;
using System.Collections;

public class Vasara : MonoBehaviour {

	private Vector3 from;
	private Vector3 to;
	private AudioSource source;
	private ParticleSystem particles;
	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource>();
		particles = GetComponentInChildren<ParticleSystem>();
		/*
		 * Positiot linecastille
		 */
		float x, y, y2;
		x = 4.0f;
		y = 2.0f;
		y2 = 7.0f;
		from = transform.position - new Vector3(x, y, 0);
		to = transform.position - new Vector3(x, y2, 0);
		/*
		 * Satunnaiset aloitusanimaatiot
		 */
		Animator anim = GetComponent<Animator>();
		anim.speed = Random.Range(0.5f, 2.5f);
		StartCoroutine(DefaultSpeed(anim));
	}
	
	IEnumerator DefaultSpeed(Animator anim) {
		yield return new WaitForSeconds(2.0f);
		anim.speed = 1.0f;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void CheckForWalnut() {
        //Katotaan onko saksanpähkinä vasaran alla
        //Jos on -> tapa
		RaycastHit2D ray = Physics2D.Linecast(from, to);
		if(ray) {
			GameObject obj = ray.collider.gameObject;
			if(obj.CompareTag("Player")) {
				obj.GetComponent<Saksanpahkina>().Kill();
			}
		}
    }
	
	void PlaySound() {
		source.Play();
		particles.Play();
	}
}
