﻿using UnityEngine;
using System.Collections;

public class Ritarihattu : AbstractHattu {
	public GameObject effect;
	public GameObject miekka;
	// Use this for initialization
	public override void Start () {
		InactiveObjects.inactiveObjects.Add(gameObject);
		hat = Saksanpahkina.Hat.knight;
		hatOffset = new Vector3(0f, 1.65f, 0f);
	}
	
	// Update is called once per frame
	public override void Update () {
	
	}
	protected override void ExtraCall(Saksanpahkina player) {
		Instantiate(effect, transform.position, transform.rotation);
		Sprite sprite = miekka.GetComponent<SpriteRenderer>().sprite;
		player.SetHatAccessory(sprite);
	}
}
