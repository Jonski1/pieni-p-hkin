﻿using UnityEngine;
using System.Collections;

public class EndTrigger : MonoBehaviour {

	bool end;
	// Use this for initialization
	void Start () {
		end = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(end) {
			if(Input.anyKey) Application.LoadLevel("mainMenu");
		}
	}
	
	void OnTriggerEnter2D(Collider2D col) {
		if(col.CompareTag("Player")) {
			Saksanpahkina s = col.gameObject.GetComponent<Saksanpahkina>();
			s.Disable();
			s.SetNoHat();
			s.SetAnim("running");
		//	s.rigidbody2D.velocity = new Vector2(35.0f, 0f);
			StartCoroutine(PositionCheck(col.gameObject));
		}
	}
	
	IEnumerator PositionCheck(GameObject obj) {
		GameObject bar = GameObject.Find("Baari");
		float x = bar.transform.position.x + 10.0f;
		while(Mathf.Abs(obj.transform.position.x - x) > 0.2f) {
		//	print(Mathf.Abs(obj.transform.position.x - x));
			obj.rigidbody2D.velocity = new Vector2(10.0f, 0f);
			yield return null;
		}
		obj.rigidbody2D.velocity = new Vector2(0f, 0f);
		GameObject.Find("EndScore").GetComponent<EndScore>().End();
		Invoke("SetEnd", 3.0f);
	}
	
	public void SetEnd() {
		end = true;
	}
}
