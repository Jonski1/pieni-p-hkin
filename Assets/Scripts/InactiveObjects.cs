﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class InactiveObjects{
	/*
		Objektit, jotka syntyy uudestaan respawnin jälkeen, tallennetaan tähän
	*/
	public static List<GameObject> inactiveObjects = new List<GameObject>();

}
