﻿using UnityEngine;
using System.Collections;



public class Elama : MonoBehaviour {

	public AudioClip Elamankerays;

	void OnTriggerEnter2D(Collider2D collider){
		if (collider.gameObject.name == "Saksanpähkinä") {
			collider.gameObject.GetComponent<Saksanpahkina>().lisaaElama();
			AudioSource.PlayClipAtPoint(Elamankerays, transform.position);
			GetComponent<ParticleSystem>().Play();
			Destroy(GetComponent<CircleCollider2D>());
			Destroy(GetComponent<SpriteRenderer>());
			Invoke ("DestroyThis", 0.3f);
		}
	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void DestroyThis() {
		Destroy(this.gameObject);
	}
}
