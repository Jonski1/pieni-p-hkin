﻿using UnityEngine;
using System.Collections;
using System;

public class Pahkinansarkija : MonoBehaviour {
	
	public float range;
	public float vel;
	private float left_bound;
	private float right_bound;
	public bool moving;
	private GameObject hitCheck;
	private Vector3 checkVector;
	// Use this for initialization
	void Start () {
		left_bound = transform.position.x - range / 2.0f;
		right_bound = transform.position.x + range / 2.0f;
		rigidbody2D.velocity = new Vector2(vel, 0f);
		Flip();
		moving = true;
		checkVector = new Vector3(0f, 0.5f, 0f);
		hitCheck = null;
		foreach(Transform trans in gameObject.GetComponentsInChildren<Transform>()) {
			if(trans.gameObject.name == "SarkijaHitCheck") {
				hitCheck = trans.gameObject;
			}
		}
		StartCoroutine(Move());
	}
	
	public void Continue() {
		rigidbody2D.velocity = new Vector2(vel, 0f);
		StartCoroutine(Move());
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	IEnumerator Move() {
		while(moving) {
			if(transform.position.x <= left_bound && vel < 0f) {
				vel *= -1.0f;
				rigidbody2D.velocity = new Vector2(vel, 0f);
				Flip();
			}
			else if(transform.position.x >= right_bound && vel > 0f) {
				vel *= -1.0f;
				rigidbody2D.velocity = new Vector2(vel, 0f);
				Flip();
			}
			yield return new WaitForSeconds(0.3f);
		}
	}
	
	void Attack() {
		RaycastHit2D ray = Physics2D.Linecast(transform.position, hitCheck.transform.position + checkVector, 1 << LayerMask.NameToLayer("Player"));
		RaycastHit2D ray2 = Physics2D.Linecast(transform.position, hitCheck.transform.position - checkVector, 1 << LayerMask.NameToLayer("Player")); 
		if(ray) {
			GameObject plr = ray.collider.gameObject;
			plr.GetComponent<Saksanpahkina>().Damage();
			float coefficient = (float)Math.Sign(plr.transform.position.x - transform.position.x);
			plr.rigidbody2D.AddForce(new Vector2(350.0f * coefficient, 500.0f));
		}
		else if(ray2) {
			GameObject plr = ray2.collider.gameObject;
			plr.GetComponent<Saksanpahkina>().Damage();
			float coefficient = (float)Math.Sign(plr.transform.position.x - transform.position.x);
			plr.rigidbody2D.AddForce(new Vector2(350.0f * coefficient, 500.0f));
		}
	}
	
	void Flip() {
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
	
}
