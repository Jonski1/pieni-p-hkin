﻿using UnityEngine;
using System.Collections;

public class KolikkoGui : MonoBehaviour {

	public Texture2D kolikko;
	public GUIText score;
	private int total;
	// Use this for initialization
	void Start () {
		score.color = Color.black;
		kolikkotoiminta.keratyt = 0;
	}
	
	// Update is called once per frame
	void Update () {
		total = kolikkotoiminta.keratyt;
	}

	void OnGUI(){
		GUI.DrawTexture (new Rect (Screen.width - 100, 15, 30, 30), kolikko);
		score.text = "X " + total;
	}
	
}
