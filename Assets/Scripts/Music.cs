﻿using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour{
	
	private AudioSource[] source;
	private bool sound;
	private int songNumber;
	
	// Use this for initialization
	void Start () {
		sound = true;
		songNumber = 0;
		source = GetComponents<AudioSource>();
		StartCoroutine(Playlist());
	}
	
	IEnumerator Playlist() {
		float delay = 5.0f;
		int len = source.Length;
		source[songNumber].Play();
		while(sound) {
			while(source[songNumber].isPlaying) {
				if(!sound) break;
				yield return new WaitForSeconds(delay);
			}
			songNumber++;
			if(songNumber >= len) songNumber = 0;
			source[songNumber].Play();
		}
		for(songNumber = 0; songNumber < len; songNumber++) {
			if(source[songNumber].isPlaying) source[songNumber].Stop();
		}
	}
	
	public void QuietDownForAWhile(float time) {
		StartCoroutine(Quiet(time));
	}
	
	IEnumerator Quiet(float time) {
		foreach(AudioSource s in source) {
			s.volume /= 2.0f;
		}
		yield return new WaitForSeconds(time);
		foreach(AudioSource s in source) {
			s.volume *= 2.0f;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
