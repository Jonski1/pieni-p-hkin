﻿using UnityEngine;
using System.Collections;

public class FadeToBlack : MonoBehaviour {
	Texture2D tex;
	Rect rect;
	float alpha;
	// Use this for initialization
	void Start () {
		tex = new Texture2D(1, 1);
		tex.SetPixel(0, 0, new Color(0f, 0f, 0f, 0f));
		tex.Apply();
		rect = new Rect(0, 0, Screen.width, Screen.height);
		alpha = 0f;
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void OnGUI() {
		alpha += Mathf.Clamp01(Time.deltaTime/10.0f);
		if(alpha > 1.0f) alpha = 1.0f;
		tex.SetPixel(0, 0, new Color(0f, 0f, 0f, alpha));
		tex.Apply();
		GUI.DrawTexture(rect, tex);
	}
	
	public void OnDestroy() {
		Destroy(tex);
	}
}
