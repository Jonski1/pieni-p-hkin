﻿using UnityEngine;
using System.Collections;

public class EndScore : MonoBehaviour {
	
	public Texture2D pullo1, pullo2, pullo3, pullo4;
	private bool end;
	private Texture2D[] texList;
	private int[] values;
	private int[] offset;
	private int x, y;
	//private GUIStyle style;
	
	// Use this for initialization
	void Start () {
		end = false;
	//	style = new GUIStyle();
	//	style.fontSize = 30;
		texList = new Texture2D[4] { pullo1, pullo2, pullo3, pullo4 };
		values = new int[4] { 200, 45, 10, 5 };
		offset = new int[4] { 40, 27, 30, 20 };
		x = Screen.width / 2 + 200;
		y = 150;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void End() {
		end = true;
	}
	
	void OnGUI() {
		if(end) {
		//	int y2 = 200;
			GUI.skin.label.fontSize = 30;
			GUI.contentColor = Color.black;
			int amount = kolikkotoiminta.keratyt;
			int times;
			GUI.Label(new Rect(400, y + 30, 600, 100), amount + " kolikkoa riittää seuraaviin virvokkeisiin:");
			int i = 0;
			for(; i < texList.Length; i++) {
				times = (amount - (amount % values[i])) / values[i];
			//	GUI.Label(new Rect(x, y * (i + 1), 10, 10), times + "x", style);
				GUI.Label(new Rect(x, y * (i + 1), 100, 100), times + "x");
				GUI.DrawTexture(new Rect(x + 80, y * (i + 1) - offset[i], texList[i].width / 2, texList[i].height / 2), texList[i]);
				amount -= times * values[i];
			}
			GUI.skin.label.fontSize = 50;
			GUI.Label(new Rect(Screen.width / 3, Screen.height - Screen.height / 5, 700, 200), "Onneksi olkoon, pääsit ehjänä perille!");
		}
	}
}
