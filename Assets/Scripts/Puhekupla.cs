﻿using UnityEngine;
using System.Collections;

public class Puhekupla : MonoBehaviour {
	
	private TextMesh t;
	private float delay;
	// Use this for initialization
	void Start () {
		t = GetComponentInChildren<TextMesh>();
		delay = 2.0f;
		string[] text = { "Huhhuh,\nhyvää huomenta!", "Päätä särkee ja\njanottaa...", "Taidanpa etsiä\nkolikoita ja\nostaa juotavaa!" };
		SayLines(text);
	}
	
	public void SayLines(string[] lines) {
		StartCoroutine(Act(lines));
		GameObject.FindWithTag("Player").GetComponent<Saksanpahkina>().Disable();
	}
	
	IEnumerator Act(string[] lines) {
		for(int i = 0; i < lines.Length; i++) {
			t.text = lines[i];
			yield return new WaitForSeconds(delay);
		}
		GameObject.FindWithTag("Player").GetComponent<Saksanpahkina>().Enable();
		Destroy(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
