﻿using UnityEngine;
using System.Collections;

public class Tappotriggeri : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter2D(Collider2D collider) {
		if(collider.CompareTag("Player")) {
			collider.gameObject.GetComponent<Saksanpahkina>().Kill();
		}
	}
}
