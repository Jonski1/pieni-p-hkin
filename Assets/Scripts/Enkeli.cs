﻿using UnityEngine;
using System.Collections;

public class Enkeli : MonoBehaviour {
	
	private float x_vel;
	private float y_vel;
	private int iterations, max_iterations;
	
	// Use this for initialization
	void Start () {
		x_vel = 2.5f;
		y_vel = 3.0f;
		iterations = 0;
		max_iterations = 90;
		rigidbody2D.velocity = new Vector2(0f, y_vel);
	}
	
	// Update is called once per frame
	void Update () {
		iterations++;
		if(iterations < max_iterations) return;
		x_vel *= -1.0f;
		rigidbody2D.velocity = new Vector2(x_vel, y_vel);
	}
}
