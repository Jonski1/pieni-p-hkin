﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class startScreen : MonoBehaviour {
	
	public bool muted;
	public Texture2D background, nappi, sound, no_sound;
	public GUIStyle tyyli;
	private Texture2D mute_nappi;

	void OnGUI () {
		
	//	GUI.skin = mainSkin;

		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), background);
		//StartButton
		if (GUI.Button (new Rect ((Screen.width/2)-50, (Screen.height/2)-10, nappi.width / 2, nappi.height / 2), nappi, tyyli)) {
			InactiveObjects.inactiveObjects = new List<GameObject>();
			Screen.showCursor = false;
			Application.LoadLevel("Level1");
		}
		GUI.Label (new Rect ((Screen.width/2)-50, (Screen.height/2)-10, nappi.width / 2, nappi.height / 2), "Aloita peli", tyyli);
		//QuitButton
		if (GUI.Button (new Rect ( 10, (Screen.height - 100), nappi.width/2, nappi.height / 2), nappi, tyyli)) {
			Application.Quit();
		}
		GUI.Label(new Rect ( 10, (Screen.height - 100), nappi.width/2, nappi.height / 2), "Lopeta peli", tyyli);
		//MuteButton
		if (GUI.Button (new Rect ((Screen.width - 200), (Screen.height - 100), mute_nappi.width/2, mute_nappi.height/2), mute_nappi, tyyli)) {
			
			if(!muted){
				AudioListener.volume = 0.0F;
				mute_nappi = no_sound;
				muted = true;
			}
			else{
				AudioListener.volume = 1.0F;
				mute_nappi = sound;
				muted = false;
			}
		}
		
		//InstructionsButton
		if (GUI.Button (new Rect ( 10, (Screen.height - 200), nappi.width / 2, nappi.height / 2), nappi, tyyli)) {
			Application.LoadLevel("ohjeruutu");
		}
		GUI.Label(new Rect ( 10, (Screen.height - 200), nappi.width / 2, nappi.height / 2), "Ohjeet", tyyli);
	}
	// Use this for initialization
	void Start () {
		muted = AudioListener.volume == 0.0f;
		if(muted) mute_nappi = no_sound;
		else {
			mute_nappi = sound;
		}
		Time.timeScale = 1.0f;
		Screen.showCursor = true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
