﻿using UnityEngine;
using System.Collections;
using System;

public class Tuhottava : MonoBehaviour {
	
	//Pitää lisätä ja luoda tuhoutumisefekti
	public GameObject tuhoutumisEfekti;
	
	// Use this for initialization
	void Start () {
		InactiveObjects.inactiveObjects.Add(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	//Katotaan onko collideri pelaaja, tuleeko se alhaalta, ja jos tulee, niin ei tule sivulta
	void OnCollisionEnter2D(Collision2D collision) {
		GameObject obj = collision.gameObject;
		float y_player = obj.transform.position.y;
		float y_object = transform.position.y;
		float delta_x = Math.Abs(obj.transform.position.x - transform.position.x);
		float x_error = renderer.bounds.size.x / 2.0f + 0.1f;
		if(obj.CompareTag("Player") && y_player < y_object && delta_x < x_error) {
			DestroyBlock();
		}
	}
	
	public void DestroyBlock() {
		Instantiate(tuhoutumisEfekti, transform.position, transform.rotation);
		gameObject.SetActive(false);
	}
}
