﻿using UnityEngine;
using System.Collections;

public class Kerattava : AbstractHattu {
	public GameObject prefabKatoaminen;
	public GameObject palokirves;
	
	// Use this for initialization
	public override void Start () {
		InactiveObjects.inactiveObjects.Add(gameObject);
		hat = Saksanpahkina.Hat.firefighter;
		hatOffset = new Vector3(0f, 1.9f, 0f);
	}
	
	// Update is called once per frame
	public override void Update () {

	}

	protected override void ExtraCall(Saksanpahkina player) {
		Instantiate(prefabKatoaminen, transform.position, transform.rotation);
		Sprite sprite = palokirves.GetComponent<SpriteRenderer>().sprite;
		player.SetHatAccessory(sprite);
	}
}
