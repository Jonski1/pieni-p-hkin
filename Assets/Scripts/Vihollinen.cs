﻿using UnityEngine;
using System.Collections;

public class Vihollinen : MonoBehaviour {
	
	public GameObject effect;
	
	void Start() {
		InactiveObjects.inactiveObjects.Add(gameObject);
	}

	public void Damage() {
		//Destroy(gameObject);
		Instantiate(effect, transform.position, transform.rotation);
		gameObject.SetActive(false);
	}
}
