﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Saksanpahkina : MonoBehaviour {
	public float horizontalForce;
	public float maxSpeed;
	public float jumpSpeed;
	public GameObject enkeli;
	public GameObject puolikas;
	public GameObject fade;
	bool grounded;
	private int jumpCounter = 0;
	private const int maxJumpCounter = 3;
	private bool disabled;
	private bool jump = false;
	private float rushCooldownTime = 1.0f;
	private bool rushCooldown = false;
	private GameObject weapon;
	private SpriteRenderer weapon_renderer;
	private List<Object> deathList;
	const string running = "running";
	const string jumping = "jumping";
    const float airForceDivider = 5.0f;
	Animator anim;
	Facing facing;
	GameObject groundCheck;
	Hat hat;
	private Vector3 defaultStart;
	private Vector3 groundCheckVector;
	private Spawnpoint point;
	public static int elamat;
	public AudioClip kuolema;
	public AudioClip hyppy;
	public GameObject respawnEfekti;
	
	enum Facing
	{ left = -1, right = 1 };
	
	public enum Hat
	{ none, firefighter, knight, rambo };
	
	// Use this for initialization
	void Start () {
		elamat = 1;
		point = null;
		anim = GetComponent<Animator>();
		facing = Facing.right;
		hat = Hat.none;
		grounded = true;
		groundCheck = GameObject.Find("GroundCheck");
		groundCheckVector = new Vector3(0.4f, 0f, 0f);
		defaultStart = transform.position;
		disabled = false;
		weapon = GameObject.Find("Ase");
		weapon_renderer = weapon.GetComponent<SpriteRenderer>();
		//Debug.Log(rigidbody2D.gravityScale);
	}
	
	// Update is called once per frame
	void Update () {
		//Älä tee mitään kuoleman jälkeen
		if(disabled) return;
		//Liikkuminen
		if(grounded) {
			SetAnim("idle");
		}
		//Hyppy
		if(Input.GetButtonDown("Jump") && grounded) {
			jump = true;
			SetAnim(jumping);
			AudioSource.PlayClipAtPoint(hyppy, transform.position);
			grounded = false;
			jumpCounter = maxJumpCounter;
			
		}
		//Hyökkäys
		if(Input.GetButtonDown("Attack")) {
			switch(hat) {
				case Hat.firefighter:
					anim.SetTrigger("Attack");
					break;
				case Hat.knight:
					anim.SetTrigger("Attack");
					break;
				case Hat.rambo:
					if(!disabled && !rushCooldown) {
						anim.SetTrigger("Rush");
						disabled = true;
						rushCooldown = true;
						rigidbody2D.gravityScale = 0f;
						rigidbody2D.velocity = new Vector2(0f, 0f);
						rigidbody2D.AddForce(new Vector2(transform.localScale.x * 1000.0f, 0f));
						StartCoroutine("Rush");
					}
					break;
				default:
					break;
			}
		}
		//Hacki, joka skippaa pari ground-checkiä hypyn jälkeen
		if(jumpCounter > 0) {
			jumpCounter--;
			return;
		}
		//Tarkistaa, koskeeko positio - groundcheckvector tai positio + groundcheckvector maata
		grounded = 
		(Physics2D.Linecast(transform.position - groundCheckVector, groundCheck.transform.position - groundCheckVector, 1 << LayerMask.NameToLayer("Ground"))
		|| Physics2D.Linecast(transform.position + groundCheckVector, groundCheck.transform.position + groundCheckVector, 1 << LayerMask.NameToLayer("Ground")));

		//Elämät gui

	}
	
	void FixedUpdate () {
		if(disabled) return;
		float dir = Input.GetAxisRaw("Horizontal");
		if(dir != 0f) {
			if(dir < 0f) {
				if(facing == Facing.right) Flip();
			}
			else {
				if(facing == Facing.left) Flip();
			}
			if(grounded) {
				SetAnim(running);
				//Liikkumisnopeuden rajoitus
				if(System.Math.Sign(dir) != System.Math.Sign(rigidbody2D.velocity.x) || System.Math.Abs(rigidbody2D.velocity.x) <= maxSpeed) rigidbody2D.AddForce(Vector2.right * dir * horizontalForce);
			//	print(rigidbody2D.velocity.x);
			}
			else {
				rigidbody2D.AddForce(Vector2.right * dir * (horizontalForce / airForceDivider));
			}
		}
		if(jump) {
			jump = false;
			rigidbody2D.AddForce(Vector2.up * jumpSpeed);
		}
			
	}
	//Asettaa animaation
	public void SetAnim(string animation) {
		switch (animation) {
			case jumping:
				anim.SetBool(jumping, true);
				anim.SetBool(running, false);
				break;
			case running:
				anim.SetBool(jumping, false);
				anim.SetBool(running, true);
				break;
			default:
				anim.SetBool(jumping, false);
				anim.SetBool(running, false);
				break;
		}
	}
	
	void Flip() {
		if(facing == Facing.left) {
			facing = Facing.right;
		}
		else {
			facing = Facing.left;
		}
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
	
	public void Kill() {
	//	Destroy(gameObject);
		AudioSource.PlayClipAtPoint(kuolema, transform.position);
		disabled = true;
		SetNoHat();
		float delay = 3.0f;
		GameObject.Find("MusicManager").GetComponent<Music>().QuietDownForAWhile(delay);
		DisableParts();
		DeathAnimation ();
		Invoke("DeathDelay", delay);
	}
	//Tyhjentää grafiikat, ottaa colliderit pois käytöstä ja pysäyttää rigidbodyn
	private void DisableParts() {
		SpriteRenderer[] spriteRenderers = GetComponentsInChildren<SpriteRenderer> ();
				foreach (SpriteRenderer renderer in spriteRenderers) {
						renderer.enabled = false;
				}
				foreach (Collider2D collider in GetComponents<Collider2D>()) {
						collider.enabled = false;
				}
				rigidbody2D.isKinematic = true;
	}
	//Ottaa tyhjennetyt asiat takaisin käyttöön
	private void EnableParts() {
		SpriteRenderer[] spriteRenderers = GetComponentsInChildren<SpriteRenderer> ();
				foreach (SpriteRenderer renderer in spriteRenderers) {
						renderer.enabled = true;
				}
				foreach (Collider2D collider in GetComponents<Collider2D>()) {
						collider.enabled = true;
				}
				rigidbody2D.isKinematic = false;
	}
	
	private void DeathAnimation() {
		deathList = new List<Object>();
		deathList.Add(Instantiate(enkeli, transform.position, transform.rotation));
		Vector3 diff = new Vector3(0.25f, 0f, 0f);
		deathList.Add(Instantiate(puolikas, transform.position - diff, Quaternion.Euler(new Vector3(0f, 0f, Random.Range(-110.0f, -75.0f)))));
		deathList.Add(Instantiate(puolikas, transform.position + diff, Quaternion.Euler(new Vector3(0f, 0f, Random.Range(75.0f, 110.0f)))));
		deathList.Add(Instantiate(fade));
	}
	
	private void DeathDelay() {
		elamat--;
		if(elamat > 0) {
			EnableParts();
			disabled = false;
		if(point != null) transform.position = point.GetComponent<Transform> ().position;
		else {
			transform.position = defaultStart;
		}
			//Poista kuolinanimaatio
			foreach(Object obj in deathList) {
				Destroy(obj);
			}
			//Respawnaa objektit
			foreach(GameObject obj in InactiveObjects.inactiveObjects) {
				if(obj != null && !obj.activeSelf) obj.SetActive(true);
				if(obj.name == "Sarkija") obj.GetComponent<Pahkinansarkija>().Continue();
			}
			Instantiate(respawnEfekti, transform.position, transform.rotation);
		}
		else {
			//Jos elämiä ei ole tarpeeksi, takaisin aloitusruutuun
			Application.LoadLevel("mainMenu");
		}
		//TODO?
		//GetComponent<ParticleSystem>().Play();
	}
	
	public void Damage() {
		SetNoHat();
		Kill();
	}
	
	public void Attack() {
		//Kuvan keskikohdasta kuvan sivureunaan, ota skaalaus mukaan
		//Siirrä checkiä alemmas animaatioframen mukaan TODO
		Vector3 from = weapon_renderer.bounds.center;
		float x = weapon_renderer.bounds.size.x - 0.2f;
		Vector3 to = weapon_renderer.bounds.center + new Vector3(x / 2.0f * transform.localScale.x, 0, 0);
		RaycastHit2D ray = Physics2D.Linecast(from, to);
		if(ray) {
			GameObject obj = ray.collider.gameObject;
			if(obj.CompareTag("Vihollinen")) {
				obj.GetComponent<Vihollinen>().Damage();
			}
			if(obj.CompareTag("Rikottava")) {
				obj.GetComponent<Tuhottava>().DestroyBlock();
			}
			if(obj.CompareTag("Liekki") && hat == Hat.firefighter) {
				obj.GetComponent<Tuhottava>().DestroyBlock();
			}
		}
	}
	
	IEnumerator Rush() {
		while(disabled) {
			Vector3 from = transform.position;
			float x = transform.position.x + (3.0f * transform.localScale.x);
			Vector3 to = new Vector3(x, transform.position.y, transform.position.z);
			{
				RaycastHit2D ray = Physics2D.Linecast(from, to, int.MaxValue - (1 << LayerMask.NameToLayer("Player")));
				if(ray) {
					GameObject obj = ray.collider.gameObject;
					if(obj.CompareTag("Vihollinen")) {
						obj.GetComponent<Vihollinen>().Damage();
					}
			/*		if(obj.CompareTag("Rikottava")) {
						obj.GetComponent<Tuhottava>().DestroyBlock();
					} */
				}
			}
			yield return new WaitForSeconds(0.1f);
		}
	}
	
	IEnumerator RushCooldown() {
		yield return new WaitForSeconds(rushCooldownTime);
		rushCooldown = false;
	}
	
	public void EndRush() {
		disabled = false;
		rigidbody2D.gravityScale = 1.0f;
		StartCoroutine("RushCooldown");
		//Debug.Log(rigidbody2D.gravityScale);
	}
	
	//Aseta hattu ja sen kuva
	public void SetHat(Hat hat, Sprite sprite, Vector3 hatOffset) {
		this.hat = hat;
		GameObject obj = GameObject.Find("Hattu");
		hatOffset.x = hatOffset.x * transform.localScale.x;
		obj.transform.position = transform.position + hatOffset;
		SpriteRenderer renderer = obj.GetComponent<SpriteRenderer>();
		renderer.sprite = sprite;
	}
	//Aseta aseen kuva
	public void SetHatAccessory(Sprite sprite) {
		weapon_renderer = weapon.GetComponent<SpriteRenderer>();
		weapon_renderer.sprite = sprite;
	}
	//Poista hattu ja ase
	public void SetNoHat() {
		SetHat(Hat.none, null, Vector3.zero);
		SetHatAccessory(null);
	}
	public void setSpanwnpoint(Spawnpoint spawnpoint){
		point = spawnpoint;
	}
	public void lisaaElama(){
			elamat++;		
	}
	
	public void Disable() {
		disabled = true;
	}
	
	public void Enable() {
		disabled = false;
	}
}
