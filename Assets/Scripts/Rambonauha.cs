﻿using UnityEngine;
using System.Collections;

public class Rambonauha : AbstractHattu {
	public GameObject effect;
	// Use this for initialization
	public override void Start () {
		InactiveObjects.inactiveObjects.Add(gameObject);
		hat = Saksanpahkina.Hat.rambo;
		hatOffset = new Vector3(-0.5f, 1.2f, 0f);
	}
	
	// Update is called once per frame
	public override void Update () {
	
	}
	protected override void ExtraCall(Saksanpahkina player) {
		Instantiate(effect, transform.position, transform.rotation);
		player.SetHatAccessory(null);
	}
}
