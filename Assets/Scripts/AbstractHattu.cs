﻿using UnityEngine;
using System.Collections;

public abstract class AbstractHattu : MonoBehaviour {

	protected Saksanpahkina.Hat hat;
	protected Vector3 hatOffset;
	public AudioClip soundi;
	// Use this for initialization
	public abstract void Start ();
	
	// Update is called once per frame
	public abstract void Update ();
	
	//Lisämetodi, josta voi ajaa hatun yksilöllistä koodia, kun hattu poimitaan
	protected virtual void ExtraCall(Saksanpahkina player) {
	
	}
	
	void OnTriggerEnter2D(Collider2D collider) {
		if(collider.CompareTag("Player")) {
			Saksanpahkina player = collider.gameObject.GetComponent<Saksanpahkina>();
			ExtraCall(player);
			AudioSource.PlayClipAtPoint(soundi, transform.position);
			Sprite sprite = GetComponent<SpriteRenderer>().sprite;
			player.SetHat(hat, sprite, hatOffset);
			//Destroy(gameObject);
			gameObject.SetActive(false);
		}

	}
}
