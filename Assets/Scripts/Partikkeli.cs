﻿using UnityEngine;
using System.Collections;

public class Partikkeli : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<ParticleSystem>().Play();
		float delay = 0.5f;
		Invoke("WaitAndDestroy", delay);
	}
	
	void WaitAndDestroy() {
		Destroy(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
