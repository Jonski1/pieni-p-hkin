﻿using UnityEngine;
using System.Collections;

public class kolikkotoiminta : MonoBehaviour {

	public static int keratyt = 0;
	public AudioClip kolikko;

	void OnTriggerEnter2D(Collider2D collider){
		if (collider.gameObject.name == "Saksanpähkinä") {
			keratyt++;
			AudioSource.PlayClipAtPoint(kolikko, transform.position);
			GetComponent<ParticleSystem>().Play();
			Destroy(GetComponent<BoxCollider2D>());
			Destroy(GetComponent<SpriteRenderer>());
			Invoke ("DestroyThis", 0.3f);
		}
	}


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	

	}

	void DestroyThis() {
		Destroy(this.gameObject);
	}
}
