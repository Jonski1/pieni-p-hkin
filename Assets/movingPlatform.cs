﻿using UnityEngine;
using System.Collections;

public class movingPlatform : MonoBehaviour {

	public float vel;
	private float y;
		
	void Start(){
		y = transform.position.y;
		float dir = System.Math.Sign(Random.Range(-1.0f, 1.0f));
		if(dir == 0f) dir = 1.0f;
		vel *= dir;
		rigidbody2D.velocity = new Vector2(vel, 0f);
	//	StartCoroutine(CheckY());
	}
	
	//Hacki, joka estää alustan uppoamisen
	IEnumerator CheckY() {
		float delay = 0.5f;
		while(gameObject.activeSelf) {
			if(transform.position.y < y) transform.position = new Vector3(transform.position.x, y, transform.position.z);
			yield return new WaitForSeconds(delay);
		}
	}
	
	void Update(){
	}
	
	void FixedUpdate() {
		
	}
	
	void OnCollisionEnter2D(Collision2D col) {
		if(col.collider.gameObject.layer == LayerMask.NameToLayer("Ground")) {
			vel *= -1.0f;
			rigidbody2D.velocity = new Vector2(vel, 0f);
		}
	}
}
